#!/bin/zsh -l 
cd ${3}/${4}/src
cmsenv
cd DQM/RPCMonitorModule/test/2nd_3rd/Express_Cosmics
cmsRun calculateEfficiency.py ${1} ${2}
