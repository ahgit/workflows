#!/bin/zsh -l 
cmssw_env_dir="${1}/${2}"

cmssw_ready=false
while [ "$cmssw_ready" = false ]; do
    if [ -d "$cmssw_env_dir" ]; then
        if [ -e "$cmssw_env_dir/ready" ]; then
            echo "CMSSW environment exists and is ready."
	    cmssw_ready=true
        else
            echo "CMSSW environment exists but is not ready."
	    echo "Retrying in 5 minutes..."
	    sleep 300
        fi
    else
        echo "CMSSW environment does not exist."
	echo "Retrying in 10 minutes..."
	sleep 600
    fi
done
