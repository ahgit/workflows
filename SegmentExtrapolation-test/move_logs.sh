#!/bin/zsh -l 
#rm -rf ${5}/run_${1}${2}
mkdir -p ${5}/run_${1}${2}/logs
cd ${3}/${4}/src
cd DQM/RPCMonitorModule/test/condor/Express_Cosmics/run_${1}${2}/test
cp * ${5}/run_${1}${2}/logs
