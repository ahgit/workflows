#!/bin/zsh -l 
# Clean files from condor submit.
cd ${3}/${4}/src
cd DQM/RPCMonitorModule/test/condor/Express_Cosmics
# Remove the given cluster of jobs.
if [ -e "submitResult_${1}${2}.txt" ]; then
	cluster_id=$(grep -o -P '(?<=cluster ).*?(?=\.)' submitResult_${1}${2}.txt)
	condor_rm $cluster_id
else
	echo "No jobs were submitted to condor."
fi

# Clean.
sleep 15
rm -rf submitResult_${1}${2}.txt
rm -rf condorQuery_${1}${2}.txt
rm -rf run_${1}${2}

