#!/bin/zsh -l 
cd ${3}
rm -rf run_${1}${2}
mkdir run_${1}${2}
cd run_${1}${2}
rm -rf ${4}
cmsrel ${4}
cd ${4}/src
cmsenv
git cms-addpkg RecoLocalMuon/RPCRecHit
git clone -b run3_offlineAna_12_5_2_aut https://gitlab.cern.ch/ahussein/OfflineRPCEfficiency.git 
cd OfflineRPCEfficiency
mv * ../
cd ../
cp /afs/cern.ch/user/m/mileva/public/rpc_dpg/RE4_run3/CSCObjectMap.cc RecoLocalMuon/RPCRecHit/plugins/
scram b -j 8
