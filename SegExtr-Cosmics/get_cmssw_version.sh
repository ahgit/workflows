#!/bin/zsh -l 
source /afs/cern.ch/work/a/ahgit/rpc-automation/rpc_tools/cms-rr-el804/bin/activate
cmssw_version=$(python /afs/cern.ch/work/a/ahgit/rpc-automation/rpc_tools/cms-rr-el804/scripts/get_cmssw_version.py --run_number="${1}${2}")
echo $cmssw_version
