#!/bin/zsh -l
cd ${4}/run_${1}${2}/
rm -rf plots
mkdir plots
cd plots
cp ../3steps/SummaryAnalyzeEfficiency_${1}${2}_Express2023.root .
cp -r ${3}/CMSSW_13_0_10/src/DQM/RPCMonitorModule/macros/Chamber_Issues .
cp ${3}/CMSSW_13_0_10/src/DQM/RPCMonitorModule/macros/Construct_bChambers.py .
cp ${3}/CMSSW_13_0_10/src/DQM/RPCMonitorModule/macros/CSVReader.h .
cp ${3}/CMSSW_13_0_10/src/DQM/RPCMonitorModule/macros/EndcapPlot.C .
cp ${3}/CMSSW_13_0_10/src/DQM/RPCMonitorModule/macros/WheelPlot.C .
cp ${3}/CMSSW_13_0_10/src/DQM/RPCMonitorModule/macros/drawEfficiencyHistograms.C .
python3 Construct_bChambers.py
summaryFile_name="SummaryAnalyzeEfficiency_${1}${2}_Express2023.root"
root -l -b -q "drawEfficiencyHistograms.C(true, true, 1, \"$summaryFile_name\")"
# Clean.
find . -type f ! -name "*.png" -delete
rm -rf Chamber_Issues
