#!/bin/zsh -l 
cd /afs/cern.ch/work/a/ahgit/rpc-automation/cmssw_envs
rm -rf ${1}
cmsrel ${1}
cd ${1}/src
cmsenv
source /cvmfs/cms.cern.ch/crab3/crab.sh
git cms-addpkg RecoLocalMuon/RPCRecHit
git clone -b run3_offlineAna_12_5_1 https://gitlab.cern.ch/ahussein/OfflineRPCEfficiency.git 
cd OfflineRPCEfficiency
mv * ../
cd ../
cp /afs/cern.ch/user/m/mileva/public/rpc_dpg/RE4_run3/CSCObjectMap.cc RecoLocalMuon/RPCRecHit/plugins/
scram b -j 8
