pipeline {
  agent {
      label 'lxplus'
  }
  parameters {
     string(name: 'RunNumber', defaultValue: '')
  }
  environment {
        def run_number1 = params.RunNumber.substring(0, 3)
        def run_number2 = params.RunNumber.substring(3, 6)
	def CMSSWs = '/afs/cern.ch/work/a/ahgit/rpc-automation/cmssw_envs'
	def RunsOutput = '/eos/user/a/ahgit/rpc-automation/runs-output/Collisions'
	def RPCTools = '/afs/cern.ch/work/a/ahgit/rpc-automation/rpc_tools'
	def JsonFiles = '/afs/cern.ch/work/a/ahgit/rpc-automation/runs_jsonFiles'
  }
  stages {
    stage('Initialization') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
	  buildName "Run${env.RunNumber}"
          buildDescription "Segment Extraolation analysis for ${env.BUILD_DISPLAY_NAME} executed @ ${NODE_NAME}"
          mattermostSend (
		color: "#2A42EE",
	 	message: "Build STARTED: ${env.JOB_NAME} (<${env.BUILD_URL}|${env.BUILD_DISPLAY_NAME}>)"
	  )
        }
      }
    }
    stage('Get CMSSW version for the run') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
          script {
             def scriptOutput = sh(script: './get_cmssw_version.sh ${run_number1} ${run_number2}', returnStdout: true).trim()
	     env.CMSSW_VER = scriptOutput
	     echo "CMSSW version for run ${run_number1}${run_number2} is ${CMSSW_VER}"
         }
        }
      }
    }
    stage('Setup CMSSW environment') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
        withCredentials([gitUsernamePassword(credentialsId: 'ahgit-userpass', gitToolName: 'Default')]) {
          script {
             sh "./setup_cmssw.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER}"
            }
          }
        }
      }
    }
    stage('Create json file for the run') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
          script {
             sh "./create_run_json.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
         }
        }
      }
    }
    stage('Submit to condor') {
      steps {
	dir('SegmentExtrapolation-Collisions_v2') {
	  script {
	     sh "./submit_condor.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
	 }
      	}
      }
    }
    stage('Move logs') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
        script {
        sh "./move_logs.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
          }
        }
      }
    }
    stage('Merge the output files') {
      steps {
	dir('SegmentExtrapolation-Collisions_v2') {
	script {
	sh "./merge.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
	  }
      	}
      }
    }
    stage('2nd step: calculate efficiency') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
        script {
        sh "./2nd.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
          }
        }
      }
    }
   stage('3rd step: make efficiency summary') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
        script {
        sh "./3rd.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
          }
        }
      }
    }
   stage('plot and publish') {
      steps {
        dir('SegmentExtrapolation-Collisions_v2') {
	  script {
	     sh "./plotNpublish.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
	 }
      	}
      }
    }
  }

 post {
        success {
            script {
                // Send a Mattermost notification when the build succeeds.
                mattermostSend(
                    color: 'good',
                    message: "Build SUCCESS: ${env.JOB_NAME} (<${env.BUILD_URL}|${env.BUILD_DISPLAY_NAME}>). You can find the plots at (<https://cernbox.cern.ch/files/spaces/eos/user/a/ahgit/rpc-automation/runs-output/run_${run_number1}${run_number2}/plots|plots>)"
                )
            }
        }
        
        failure {
            script {
                // Send a Mattermost notification when the build fails.
                mattermostSend(
                    color: 'danger',
                    message: "Build FAILED: ${env.JOB_NAME} (<${env.BUILD_URL}|${env.BUILD_DISPLAY_NAME}>)"
                )
            }
        }
	always {
	    dir('SegmentExtrapolation-Collisions_v2') {
            	script {
                    sh "./clean.sh ${run_number1} ${run_number2} ${CMSSWs} ${CMSSW_VER} ${RunsOutput}"
            	}
	    }
        }
    }
}
