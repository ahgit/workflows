#!/bin/zsh -l 
# Clean files from condor submit.
cd ${3}/run_${1}${2}/${4}/src
cd DQM/RPCMonitorModule/test/condor/RPCMon
# Remove the given cluster of jobs.
if [ -e "submitResult_${1}${2}.txt" ]; then
	cluster_id=$(grep -o -P '(?<=cluster ).*?(?=\.)' submitResult_${1}${2}.txt)
	condor_rm $cluster_id
else
	echo "All jobs finished for this run or No jobs were submitted at all."
fi

# Clean.
sleep 15
rm -rf submitResult_${1}${2}.txt
rm -rf condorQuery_${1}${2}.txt
rm -rf run_${1}${2}
cd ${3}
rm -rf run_${1}${2}
