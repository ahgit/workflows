#!/bin/zsh -l

dir_size=50
dir_name="output"

cd ${3}/run_${1}${2}/${4}/src
cmsenv
cd DQM/RPCMonitorModule/test/condor/RPCMon_RAW/run_${1}${2}/output_${1}${2}

n=$((`find . -maxdepth 1 -type f | wc -l`/$dir_size+1))
n_files=$((`find . -maxdepth 1 -type f | wc -l`))
echo "Number of files = $n_files"
echo "Number of divisions = $n"

if [ "$n" -gt 1 ]; then
	echo "Dividing..."
	# Divide step.
	for i in `seq 1 $n`;
	do
		mkdir -p "$dir_name$i";
		find . -maxdepth 1 -type f | head -n $dir_size | xargs -i mv "{}" "$dir_name$i"
	done

	# Merge: step1.
	echo "Merging: Step1..."
	for i in `seq 1 $n`;
	do
		cd $dir_name$i
		hadd -j 8 -f -O $dir_name$i.root *.root
		cp $dir_name$i.root ../
		rm -rf *.root
		cd ../
	done
fi

# Merge: step2. 
echo "Merging: Step2..."
hadd -j 8 -f -O Efficiency_${1}${2}_RPCMon2024.root *.root

# Move to EOS.
mv Efficiency_${1}${2}_RPCMon2024.root ../../../../2nd_3rd/RPCMon/
