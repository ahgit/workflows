#!/bin/zsh -l

source /afs/cern.ch/work/a/ahgit/rpc-automation/rpc_tools/cms-rr-el804/bin/activate
cd /afs/cern.ch/work/a/ahgit/rpc-automation/runs_jsonFiles
python /afs/cern.ch/work/a/ahgit/rpc-automation/rpc_tools/cms-rr-el804/scripts/create_json.py --run_number=${1}${2}
# Copy json file to EOS.
rm -rf ${5}/run_${1}${2}
mkdir -p ${5}/run_${1}${2}/json
cp ${1}${2}.txt ${5}/run_${1}${2}/json
