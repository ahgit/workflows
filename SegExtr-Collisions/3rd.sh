#!/bin/zsh -l 

cd ${3}/run_${1}${2}/${4}/src
cmsenv
cd DQM/RPCMonitorModule/test/2nd_3rd/RPCMon
cmsRun makeEfficiencySummary.py ${1} ${2}
mkdir -p ${5}/run_${1}${2}/3steps
mv *${1}${2}*.root ${5}/run_${1}${2}/3steps
