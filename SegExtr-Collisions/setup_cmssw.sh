#!/bin/zsh -l 

cd ${3}
rm -rf run_${1}${2}
mkdir run_${1}${2}
cd run_${1}${2}
rm -rf ${4}
cmsrel ${4}
cd ${4}/src
cmsenv
git clone -b run3_offlineAna_12_5_3 https://gitlab.cern.ch/ahussein/OfflineRPCEfficiency.git 
cd OfflineRPCEfficiency
mv * ../
cd ../
rm -rf OfflineRPCEfficiency
scram b -j 8
