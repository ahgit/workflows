#!/bin/zsh -l 

cd ${3}/run_${1}${2}/${4}/src
cmsenv
cd DQM/RPCMonitorModule/test/condor/RPCMon_RAW/run_${1}${2}/output_${1}${2}
hadd -j 8 -f -O Efficiency_${1}${2}_RPCMon2024.root *.root
mv Efficiency_${1}${2}_RPCMon2024.root ../../../../2nd_3rd/RPCMon/
