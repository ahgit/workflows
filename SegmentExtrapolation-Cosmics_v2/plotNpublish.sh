#!/bin/zsh -l
cd ${5}/run_${1}${2}/
rm -rf plots
mkdir plots
cd plots
cp ../3steps/SummaryAnalyzeEfficiency_${1}${2}_Express2023.root .
cp -r ${3}/run_${1}${2}/${4}/src/DQM/RPCMonitorModule/macros/Chamber_Issues .
cp ${3}/run_${1}${2}/${4}/src/DQM/RPCMonitorModule/macros/Construct_bChambers.py .
cp ${3}/run_${1}${2}/${4}/src/DQM/RPCMonitorModule/macros/CSVReader.h .
cp ${3}/run_${1}${2}/${4}/src/DQM/RPCMonitorModule/macros/EndcapPlot.C .
cp ${3}/run_${1}${2}/${4}/src/DQM/RPCMonitorModule/macros/WheelPlot.C .
cp ${3}/run_${1}${2}/${4}/src/DQM/RPCMonitorModule/macros/drawEfficiencyHistograms.C .
summaryFile_name="SummaryAnalyzeEfficiency_${1}${2}_Express2023.root"
python3 Construct_bChambers.py
root -l -b -q "drawEfficiencyHistograms.C(true, true, 1, \"$summaryFile_name\")"
# Clean.
find . -type f ! -name "*.png" ! -name "*.log" -delete
rm -rf Chamber_Issues
