#!/bin/zsh -l 
cd ${3}/run_${1}${2}/${4}/src
cd DQM/RPCMonitorModule/test/condor/Express_Cosmics/run_${1}${2}/output_${1}${2}
hadd -j 8 -f -O Efficiency_${1}${2}_Express2023.root *.root
mv Efficiency_${1}${2}_Express2023.root ../../../../2nd_3rd/Express_Cosmics/
