#!/bin/zsh -l 
cd ${3}/${4}/src
cd DQM/RPCMonitorModule/test/condor/RPCMon/run_${1}${2}/output_${1}${2}
hadd -j 8 -f -O Efficiency_${1}${2}_RPCMon2023.root *.root
mv Efficiency_${1}${2}_RPCMon2023.root ../../../../2nd_3rd/RPCMon/
