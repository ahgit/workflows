#!/bin/zsh -l

cd ${1}/cms-rr-el804/scripts/check-trigger/ions
first_run=$(head -n 1 "ready_runs_ions.txt")
sed -i '1d' "ready_runs_ions.txt"
echo $first_run
