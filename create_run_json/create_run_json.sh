#!/bin/zsh -l
source /afs/cern.ch/user/a/ahgit/cms-rr/rr/bin/activate
cd /afs/cern.ch/user/a/ahgit/cms-rr/rr
mkdir -p json_files/run_${1}${2}
cd json_files/run_${1}${2}
python /afs/cern.ch/user/a/ahgit/cms-rr/rr/test/create_json.py --run_number=${1}${2}
